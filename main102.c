#include "transactions.h"

int main(int argc , char *argv)
{

    int account;
    char name[30];
    double balance;
    double amount;
    fileExist(OLDMAST);
    fileExist(TRANS);
    accountRecord ac_rec;
   
    printf("Sample data for the file oldmast.dat:\n");
    printf("Enter the account, name, and balance (EOF to end):");

    while(scanf("%d%[^0-9-]%lf", &account, name, &balance) != EOF) {
        ac_rec = build_account_record(account , name , balance , 0.00);
        write_store_account(ac_rec);
        printf("Enter the account, name, and balance (EOF to end):");
    }
  

    printf("\nSample data for file trans.dat:\n");
    printf("Enter the account and transaction amount (EOF to end):");
    while(scanf("%d%lf", &account, &amount) != EOF) {
        write_single_trans_actions(account,amount);
    }
    return 0;
}


