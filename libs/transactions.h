#ifndef TRANSACTIONS_FILE
#define TRANSACTIONS_FILE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#define OLDMAST "oldmast.dat"
#define TRANS "trans.dat"
#define TRANS_LIMIT 100
FILE *trans_ofPtr;
FILE *trans_tfPtr;
typedef struct transaction{
double amount;
} transaction;

typedef struct record{
int account;
char name[30];
double balance;
transaction trans[TRANS_LIMIT] ;
} accountRecord;


void fileExist( char fname[]){
    if( access( fname, F_OK ) != -1 ) {
        printf ("Datastore File %s Exist \n" , fname);
    } else {
        printf("Creating Data Store Files %s \n", fname);
         trans_ofPtr  = fopen(fname, "w");
         fclose(trans_ofPtr);
        puts("Done");
    }
}

void write_store_account(accountRecord ac_rec){
    trans_ofPtr = fopen(OLDMAST, "a+");
    fprintf(trans_ofPtr, "%d:%s:%.2f\n", ac_rec.account,ac_rec.name, ac_rec.balance);
    fclose(trans_ofPtr);
}


void write_trans_actions(accountRecord ac_rec){
    trans_tfPtr  = fopen(TRANS, "a+");
     for(int x = 0 ; x < TRANS_LIMIT; x++){
        if(ac_rec.trans[x].amount > 0){
        fprintf(trans_tfPtr, "%d:%.2f\n", ac_rec.account, ac_rec.trans[x].amount);
        }else{
            break;
        }
     }

    fclose(trans_tfPtr);
}

void write_single_trans_actions(int account, double ammt){
    trans_tfPtr  = fopen(TRANS, "a+");
    fprintf(trans_tfPtr, "%d:%.2f\n", account, ammt);
    fclose(trans_tfPtr);
}

accountRecord build_account_record(int acct , char name[30] , double bal , double trans1){
    accountRecord ac_rec;
    ac_rec.account = acct;
    ac_rec.balance = bal;
    strcpy(ac_rec.name,name);
    ac_rec.trans[0].amount = trans1;
    for(int x = 1 ; x < TRANS_LIMIT; x++){
        ac_rec.trans[x].amount = 0; 
    }
    return ac_rec;
}

void printRecord(accountRecord ac_rec){
  for(int x = 0 ; x < TRANS_LIMIT; x++ ){
    if(ac_rec.trans[x].amount > 0){
    printf("%d %s %.2f %.2f \n", ac_rec.account, ac_rec.name,ac_rec.balance, ac_rec.trans[x].amount);
    } 
  }
}

#endif